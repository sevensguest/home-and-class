
class Game {
    currentCell = null;
    interval = null;
    computerInt = null;
    userStep = 0;
    botStep = 0;
    speed = 0;
    stop = false;

    startBtn = document.getElementById("start");
    stopBtn = document.getElementById("pause");
    refreshBtn = document.getElementById("refresh");

    getSpeed(){
        this.speed = Number(document.getElementById('speed').value);
        return true;
    }

    clearTimeInterval() {
        clearInterval(this.interval);
        clearInterval(this.computerInt);
    }

    getCell(){
        let cell = document.querySelectorAll("td:not(.red):not(.green):not(.blue)");
        let cellNumber = Math.floor(Math.random() * cell.length);

        if(cell.length == 0){
            if(confirm('Restart Game?')){
                this.clearTimeInterval();
                this.refreshItn();
            }
        }
       return this.currentCell = cell[cellNumber];
    }

    int(){
        this.stop = false;
        if(this.getSpeed()){
            this.interval = setTimeout(this.gameIteration.bind(this), this.speed);
        }
    }

     gameIteration(){
        this.getCell();
        this.currentCell.classList.add("blue");
        this.computerInt = setTimeout(()=>{
            if(this.currentCell.classList.contains("blue"))
            this.currentCell.classList.remove("blue");
            this.currentCell.classList.add("red");
            if(!this.currentCell.classList.contains('green')){
            this.botStep += 1;
            this.getResult(this.botStep);
            }
            if(this.stop === false){
            this.int();
            }
        }, this.speed + 500);
         this.currentCell.addEventListener("click", this.handlelUserClick.bind(this));

    }
    refresh(){

        let allCells = document.querySelectorAll(".red, .blue, .green");
        allCells.forEach(elem => elem.className = '');
        this.clearTimeInterval();
        this.botStep = 0;
        this.userStep = 0;
        this.getResult(0,0);
        this.stop = true;
    }

    handlelUserClick(e){
        if(e.target.classList.contains("blue")){
            e.target.className = "green";
            this.userStep += 1;
            this.getResult(this.userStep);
           }
    }

    getResult(){
          let user = document.getElementById("user");
          user.innerHTML =+ this.userStep + '';
          let computer = document.getElementById("computer");
          computer.innerHTML = this.botStep + "";

          if(this.userStep == 50){
            alert('You are winner!');
              this.refresh();


          }
          if(this.botStep == 50){
              alert('Computer wins!');
              this.refresh();


          }
      };

    getInit(){

        this.startBtn.addEventListener("click", ()=>{
            this.int();
        });

        this.stopBtn.addEventListener("click", ()=>{
            this.clearTimeInterval();
        });

        this.refreshBtn.addEventListener("click", ()=>{
            this.refresh();
        });
        }
}

let game = new Game();
game.getInit();
