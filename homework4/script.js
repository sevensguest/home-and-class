class Column {

        card = [];
        column = document.createElement('div');
        container = document.createElement('div');
        addCardBtn = document.createElement('a');
        sortCardBtn = document.createElement('a');

    render() {
        this.column.classList.add('column');
        this.column.addEventListener("dragover", this.dragOver.bind(this));
        this.column.addEventListener("drop", this.drop.bind(this));

        this.addCardBtn.classList.add('button', 'add_card');
        this.addCardBtn.innerText = 'Add new card';
        this.addCardBtn.addEventListener('click', this.addCard.bind(this));

        this.sortCardBtn.classList.add('button', 'sort_card');
        this.sortCardBtn.innerText = 'Sort A-Z ⥱';
        this.sortCardBtn.addEventListener('click', this.sort.bind(this));

        this.column.append(this.sortCardBtn);
        this.column.append(this.addCardBtn);

        this.container.classList.add('container');
        this.column.append(this.container);

        this.addCard();
        return this.column;
    }

    sort(e) {
        let column = e.target.closest('.column');
        let container = column.querySelector('.container');
        let cards = container.querySelectorAll('.card');

        let cardsArr = Array.from(cards);
         cardsArr.sort((a, b) => (a.value > b.value) ? 1 : -1);

        cardsArr.forEach(item => {
            container.appendChild(item);
        });





    }
    addCard() {
        let ident = document.getElementsByClassName('card').length;
        this.card = document.createElement('textarea');
        this.card.classList.add('card');
        this.card.id = `card-${ident}`;
        this.card.setAttribute('draggable', true);
        this.card.addEventListener('dragstart', this.dragStart.bind(this));
        this.container.append(this.card);
    }

    dragStart(e){
        e.dataTransfer.setData("Text", e.target.getAttribute('id'));
        return true;
    }

    dragOver(e){
        e.stopPropagation();
        e.preventDefault();
    }

    drop(e){
        let data = e.dataTransfer.getData("Text");

        console.log(e.target);
        if (e.target.className === "column") {
            e.target.appendChild(document.getElementById(data));
            return false;
        }
    }
}

let columns = [];

function newCol() {
    columns[columns.length] = new Column();
    document.body.append(columns[columns.length - 1].render());
}

newCol();
let newColBtn = document.querySelector('.add_col');
newColBtn.addEventListener('click', newCol);
