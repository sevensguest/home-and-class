
// Обработчик событий это функция, которая будет запускаться при срабатывании события.


function active() {
    price.className = 'active';
    warning.innerText = "";
}

function hide() {
 if (document.getElementById('information'))  information.remove();
}

function inactive() {

    if (price.value < 0) {
        price.className = 'red';
        warning.innerText = "Please enter correct price";


    } else {

        price.className = 'inactive';
        let span = document.createElement("span");
        span.id = "information";
        document.body.prepend(span);
        span.innerHTML = `Текущая цена: ${price.value} &nbsp; <a href="#" onclick="hide()">X</a><br>`;

    }}
