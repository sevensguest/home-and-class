const cities = ['hello', 'world', 'Kiev', 'Kharkiv', 'Odessa', 'Lviv'];

function list(cities) {
    document.write('<ul>');
    let newCities = cities.map(city => document.write(`<li>${city}</li>`));
    document.write('</ul>');
}

list(cities);

var timer = document.getElementById("timer");

// let i = 0;
// function showTime(i) {
//     timer.innerHTML = i;
// }
//
// do {
//     setTimeout(showTime, 1000, i);
//     i++;
// } while (i < 11);

let i = 10;

setInterval(function() {
    timer.innerHTML = i;
    if (i === 0) document.body.innerHTML = '';
    i--;
}, 1000);


