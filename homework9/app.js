const contactBtn = document.getElementById("contactBtn");

contactBtn.addEventListener("click", (e) => {
    e.preventDefault();
    document.cookie = "experiment=novalue; max-age=300";


    const allCookies = document.cookie;
    if (allCookies.includes("new-user=true")) {
        document.cookie = "new-user=false";

    }

    if (!allCookies.includes("new-user=false")) {
        document.cookie = "new-user=true";
    }
    console.log(document.cookie);

})