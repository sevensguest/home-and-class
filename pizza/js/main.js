let navBtn = document.getElementById('nav-btn');

navBtn.addEventListener('click', function() {
  document.getElementById('nav-icon').classList.toggle('fa-times');
  document.getElementById('nav-menu').classList.toggle('active');
});