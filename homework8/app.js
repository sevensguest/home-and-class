const btn = document.getElementById("btn");
btn.addEventListener("click", getLocation);

async function getLocation() {

    const {data: request} = await axios.get("https://api.ipify.org/?format=json");
    const url = `http://ip-api.com/json/${request.ip}?fields=continent,country,regionName,city,zip,district&lang=ru`;
    const {data: ipRequest} = await axios.get(url);

    const info = `
        <p><strong>Континент:</strong> ${ipRequest.continent}<br>
        <strong>Страна:</strong> ${ipRequest.country}<br>
        <strong>Регион:</strong> ${ipRequest.regionName}<br>
        <strong>Город:</strong> ${ipRequest.city}<br>
        <strong>Индекс:</strong> ${ipRequest.zip}</p>
                 `;
    btn.insertAdjacentHTML("afterend", info)
}