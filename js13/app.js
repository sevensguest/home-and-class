let cssScheme = localStorage.getItem('scheme');
if (!cssScheme) cssScheme = "main.css";

let css = '<link rel="stylesheet" href="'+cssScheme+'">';

document.head.insertAdjacentHTML("beforeEnd", css);

// console.log(localStorage.getItem('scheme'));

function original() {
    localStorage.setItem('scheme', 'main.css');
    window.location.reload(false);
}

function alt() {
    localStorage.setItem('scheme', 'alt.css');
    window.location.reload(false);
}