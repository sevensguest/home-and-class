var list = document.getElementsByClassName("tabs-title");

for (let i = 0; i < list.length; i++) {

    list[i].onclick = function () {

        document.getElementsByClassName('active')[0].classList.remove('active');
        this.classList.add('active');

        let elements = document.querySelectorAll('.tabs-content > li');
        for (let el = 0; el < elements.length; el++) {
            elements[el].classList.add('hidden');
        }
        let index = $(this).parent().children().index(this);
        elements[index].classList.remove('hidden');

    };

}

