function toggle(id) {
    let field = document.getElementById(id);
    let icon = id + "-icon";
    icon = document.getElementById(icon);

    if (field.type === "password") {
        field.type = "text";
        icon.className = "fas fa-eye-slash icon-password";
    } else {
        field.type = "password";
        icon.className = "fas fa-eye icon-password";
    }
}

function checkPass(){

    if (document.getElementById("first-pass").value == document.getElementById("second-pass").value) {
        alert("You are welcome");
    } else {
        alert("Нужно ввести одинаковые значения");
    }

}