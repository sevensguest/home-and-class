$(".image-to-show").hide();
let i = 0;
let animation = true;

function showImages() {
if (animation) {
    $(".image-to-show").eq(i-1).hide();
    $(".image-to-show").eq(i).show();
    i++;
    let timerId = setTimeout("showImages()", 2000);
    if (i === 4) i = 0;
}}

function stop(){
    animation = false;
}
function start(){
    animation = true;
    showImages();
}
